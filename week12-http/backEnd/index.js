const express = require("express");

// package for form data and json
const bodyParser = require("body-parser");

// package for upload file
const multer = require("multer");
const path = require("path");

// cors
var cors = require("cors");

// create instance
const app = express();
const port = 3000;

// config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/delay", (req, res) => {
  setInterval(function () {
    res.end(" World");
  }, 8000);
});

// example 1
app.post("/", (req, res) => {
  console.log(req.body);
  res.send("Hello Form");
});

// example 2
app.post("/upload", async (req, res) => {
  let storage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, path.join(`./upload`));
    },
    filename: (req, file, callback) => {
      const match = ["image/png", "image/jpeg"];

      if (match.indexOf(file.mimetype) === -1) {
        var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
        return callback(message, null);
      }

      var filename = `${Date.now()}-mobile-paradigm-${file.originalname}`;
      callback(null, filename);
    },
  });

  var uploadFiles = multer({ storage: storage }).array("files", 10);

  await uploadFiles(req, res, (err) => {
    console.log(req.files, req.body);
    if (err) {
      return res.send("Error uploading file.");
    }
    res.send("File is uploaded");
  });
});

app.use(require("./postController"));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
