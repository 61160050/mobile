function onKeypress(event) {
    console.log(event);
}

dragon.addEventListener('keypress', onKeypress);


function geeting(greeterFunction) {
    greeterFunction();
}

const worldGreeting = function() {
    console.log('Hello World');
}

// Arrow Function
const hawaiiaGreeting = () => {
    console.log('aloha');
}

greetings(worldGreeting);
greetings(hawaiiaGreeting);