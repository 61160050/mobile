var router = require("express").Router();

var postList = [
  {
    text: "Hello",
  },
  {
    text: "Mobile Paradigm",
  },
];

router.get("/post-list", (req, res) => {
  res.json(postList);
});

router.post("/post-list", (req, res) => {
  postList.push(req.body);
  res.json({"status": "success"});
});

module.exports = router;
