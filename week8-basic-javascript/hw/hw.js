

// 1. 
function hw1(name , num) {
    if(num === 1){
        return "Hello " + name;
    }else{
        return "Bye " + name;
    }
}

// 2.
function hw2(num) {
    n = num.toString();
    let nRev = "";
    for (let i = n.length-1 ; i >= 0; i--) {
        nRev += n[i];  
    }
    if( n == nRev){
        return true;
    }else{
        return false;
    }
} 

function hw3(n , start , end){
    if(n >= start && n < end){
        return true;
    }else{
        return false;
    }
}

function hw4(str){ 
    let count = 0;
    let temp = "";
    let show = "";
    for (let i = 0; i < str.length; i++) {
        if(str[i] != " "){
            temp += str[i];
            count++;
        }else{
            if(count <= 4){
                show += temp + " ";
                temp = "";
                count = 0;
            }else{
                for (let j = 0; j < temp.length; j++) {
                    show += '*'
                }
                show += ' ';
                temp = "";
                count = 0; 
            }
        }
        if(i == (str.length-1)){
            if(count <= 4){
                show += temp + " ";
                temp = "";
                count = 0;
            }else{
                for (let j = 0; j < temp.length; j++) {
                    show += '*'
                }
                show += ' ';
                temp = "";
                count = 0; 
            } 
        }            
    }
    return show;
}

//5 , 6
function hw5(n){
    return n * 60 * 60;
}

function hw7(str){
    let spstr = "";
    for (let i = 0; i < str.length; i++) {
        switch (str[i]) {
            case 'a':
            spstr += 1;  
            break;
            case 'e':
            spstr += 2;  
            break;
            case 'i':
            spstr += 3;  
            break;
            case 'o':
            spstr += 4;  
            break;
            case 'u':
            spstr += 5;  
            break;
            default:
            spstr +=  str[i];
            break;
        }
    }
    return spstr;
}

function hw8(str){
    spstr = "";
    for (let i = 0; i < str.length; i++) {
        if(i === 0){
            upper = str[i].toUpperCase();
            spstr += upper;
        }else if(str[i-1] == ' '){
            upper = str[i].toUpperCase();
            spstr += upper;
        }else{
            spstr += str[i];
        }
    }
    return spstr;

}

function hw9(num){
    let show = "";
    const str = num.toString();
    let count = 0;
    let strR = "";
    for (let i = str.length-1 ; i >= 0 ; i--) {
        if (count === 3) {
            count = 1;
            strR += ',' + str[i];
        }else {
            strR += str[i];
            count++;
        }
    }
    for (let j = strR.length-1 ; j >= 0 ; j--) {
        show += strR[j];
    }

        return show;
}

function hw10(p1 , p2){
    if(p1 == p2){
        return 'It’s a draw';
    }else if(p1 == 'Scissors'){
        if(p2 == 'Paper'){
            return 'The winner is p1';
        }else{
            return 'The winner is p2';
        }
    }else if(p1 == 'Paper'){
        if(p2 == 'Rock'){
            return 'The winner is p1';
        }else{
            return 'The winner is p2';
        }
    }else if(p1 === 'Rock'){
        if(p2 == 'Scissors'){
            return 'The winner is p1';
        }else{
            return 'The winner is p2';
        }
    }else{
        return "Something Wrong!" ;
    }
}

//11,12
function hw11(str){
    let first = "";
    let sec = "" ;
    for (let i = 0; i < str.length; i++) {
        let before = str[i];
        let after = str[i].toUpperCase();
        if(before == after){
            first += str[i];
        }else{
            sec += str[i];
        }
    }

    return first + sec;
    
}

function hw13(str){
    let sect = 0;
    let num1 = "";
    let num2 = "";
    let opara = "";
    for (let i = 0; i < str.length; i++) {
        if(str[i] != ' ' && sect == 0){
            num1 += str[i];
        }else if(str[i] == ' '){
            if(sect == 0){
                sect = 1;
            }else if(sect == 2){
                sect = 3 ;
            }
        }else if(sect == 1){
            opara += str[i];
            sect = 2;
        }else if(sect == 3){
            num2 += str[i];
        }
    }
    let num11 = parseInt(num1);
    let num22 = parseInt(num2);

    switch (opara) {
        case '+':
            return num11 + num22;
        break;
        case '-':
            return num11 - num22;
        break;
        case '*':
            return num11 * num22;
        break;
        case '/':
            return num11 / num22;
        break;
        default:
            return ;
        break;
    }
}

function hw14(str){
    let sect = 0;
    let num1 = "";
    let num2 = "";
    let opara = "";
    for (let i = 0; i < str.length; i++) {
        if(str[i] != ' ' && sect == 0){
            num1 += str[i];
        }else if(str[i] == ' '){
            if(sect == 0){
                sect = 1;
            }else if(sect == 2){
                sect = 3 ;
            }
        }else if(sect == 1){
            opara += str[i];
            if(str[i+1] == ' ' ){
            sect = 2;
            }
        }else if(sect == 3){
            num2 += str[i];
        }
    }

    let num11 = parseInt(num1);
    let num22 = parseInt(num2);

    switch (opara) {
        case '<':
            return num11 < num22;
        break;
        case '>':
            return num11 > num22;
        break;
        case '<=':
            return num11 <= num22;
        break;
        case '>=':
            return num11 >= num22;
        break;
        case '==':
            return num11 == num22;
        break;
        case '!=':
            return num11 != num22;
        break;
        default:
            return ;
        break;
    }
}

function hw15(str){
    let num = "";
    let unit = "";

    for (let i = 0; i < str.length; i++) {
        if(str[i] == 'C' || str[i] == 'F'){
            unit += str[i];
        }else if(str[i] == '°'){
        }else{
            num += str[i];
        }  
    }
    let temp = parseInt(num);

    if(unit == ""){
        return "error";
    }else if(unit == 'C'){
        let oc =  temp * (9/5) + 32;
        let b = oc.toFixed(0)
        return b + "°F";
    }else{
        let of = (temp - 32) * (5/9);
        let a = of.toFixed(0);
        return a + "°C";
    }

}

// main
function main() {
    result = hw15('19');
    console.log(result);
}

main();